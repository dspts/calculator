/*
 * Implement all your JavaScript in this file!
 */
"use strict";

var operation;
var inNumber;
var equalsHitted;
var anotherOp;
var lastOperation;
var lastOperator;
var res;

operation = "";
inNumber = false;
equalsHitted = false;
anotherOp = false;
lastOperation = "";
lastOperator = "";
res = "";


$("#button0").click(function(){
  anotherOp = false;
  if (equalsHitted == false){
    if (inNumber === true) {
       operation += "0";
       lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
       $("#display").val($("#display").val() + "0");
    }
    else {
      inNumber = true;
      operation += "0";
      lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
      $("#display").val("0");
    }
  } else {
    equalsHitted = false;
    inNumber = true;
    operation += "0";
    lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
    $("#display").val("0");
  }
 });

$("#button1").click(function(){
  anotherOp = false;
  if (equalsHitted == false){
    if (inNumber === true) {
       operation += "1";
       lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
       $("#display").val($("#display").val() + "1");
    }
    else {
      inNumber = true;
      operation += $(this).val();
      lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
      $("#display").val("1");
    }
  } else {
    equalsHitted = false;
    inNumber = true;
    operation += "1";
    lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
    $("#display").val("1");
  }
 });

$("#button2").click(function(){
  anotherOp = false;
  if (equalsHitted == false){
    if (inNumber === true) {
       operation += "2";
       lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
       $("#display").val($("#display").val() + "2");
    }
    else {
      inNumber = true;
      operation += "2";
      lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
      $("#display").val("2");
    }
  } else {
    equalsHitted = false;
    inNumber = true;
    operation += "2";
    lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
    $("#display").val("2");
  }
 });


$("#button3").click(function(){
  anotherOp = false;
  if (equalsHitted == false){
    if (inNumber === true) {
       operation += "3";
       lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
       $("#display").val($("#display").val() + "3");
    }
    else {
      inNumber = true;
      operation += "3";
      lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
      $("#display").val("3");
    }
  } else {
    equalsHitted = false;
    inNumber = true;
    operation += "3";
    lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
    $("#display").val("3");
  }
 });


$("#button4").click(function(){
  anotherOp = false;
  if (equalsHitted == false){
    if (inNumber === true) {
       operation += "4";
       lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
       $("#display").val($("#display").val() + "4");
    }
    else {
      inNumber = true;
      operation += "4";
      lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
      $("#display").val("4");
    }
  } else {
    equalsHitted = false;
    inNumber = true;
    operation += "4";
    lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
    $("#display").val("4");
  }
 });


$("#button5").click(function(){
  anotherOp = false;
  if (equalsHitted == false){
    if (inNumber === true) {
       operation += "5";
       lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
       $("#display").val($("#display").val() + "5");
    }
    else {
      inNumber = true;
      operation += "5";
      lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
      $("#display").val("5");
    }
  } else {
    equalsHitted = false;
    inNumber = true;
    operation += "5";
    lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
    $("#display").val("5");
  }
 });


$("#button6").click(function(){
  anotherOp = false;
  if (equalsHitted == false){
    if (inNumber === true) {
       operation += "6";
       lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
       $("#display").val($("#display").val() + "6");
    }
    else {
      inNumber = true;
      operation += "6";
      lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
      $("#display").val("6");
    }
  } else {
    equalsHitted = false;
    inNumber = true;
    operation += "6";
    lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
    $("#display").val("6");
  }
 });

$("#button7").click(function(){
  anotherOp = false;
  if (equalsHitted == false){
    if (inNumber === true) {
       operation += "7";
       lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
       $("#display").val($("#display").val() + "7");
    }
    else {
      inNumber = true;
      operation += "7";
      lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
      $("#display").val("7");
    }
  } else {
    equalsHitted = false;
    inNumber = true;
    operation += "7";
    lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
    $("#display").val("7");
  }
 });

$("#button8").click(function(){
  anotherOp = false;
  if (equalsHitted == false){
    if (inNumber === true) {
       operation += "8";
       lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
       $("#display").val($("#display").val() + "8");
    }
    else {
      inNumber = true;
      operation += "8";
      lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
      $("#display").val("8");
    }
  } else {
    equalsHitted = false;
    inNumber = true;
    operation += "8";
    lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
    $("#display").val("8");
  }
 });

$("#button9").click(function(){
  anotherOp = false;
  if (equalsHitted == false){
    if (inNumber === true) {
       operation += "9";
       lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
       $("#display").val($("#display").val() + "9");
    }
    else {
      inNumber = true;
      operation += "9";
      lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
      $("#display").val("9");
    }
  } else {
    equalsHitted = false;
    inNumber = true;
    operation += "9";
    lastOperator = operation.substring(operation.search(/[+ \- * \\/]/ ) +1 ); //
    $("#display").val("9");
  }
 });


$("#addButton").click(function(){
    inNumber = false;
    // after equals has been pressed
    if (operation == ""){
      operation += $("#display").val();
    } else if (operation.match(/[+,-]?[0-9]+.[0-9]+/)){
      operation = eval(operation);
      $("#display").val(eval(operation));
    }
    if (anotherOp == true){
      operation = String(operation).replace(/.$/, "+");
    }
    anotherOp = true;
    if (String(operation).substring(operation.length - 1) == "+" || String(operation).substring(operation.length - 1) == "-" || String(operation).substring(operation.length - 1) == "*" || String(operation).substring(operation.length - 1) == "/"){
        operation = String(operation).replace(/.$/, "+");
    }
    else {
        operation = eval(operation) + "+";
    }
    lastOperation = "+";
});

$("#subtractButton").click(function(){
    inNumber = false;
    // after equals has been pressed
    if (operation == ""){
      operation += $("#display").val();
    } else if (operation.match(/[+,-]?[0-9]+.[0-9]+/)){
      operation = eval(operation);
      $("#display").val(eval(operation));
    }
    if (anotherOp == true){
      operation = String(operation).replace(/.$/, "-");
    }
    anotherOp = true;
    if (operation.substring(operation.length - 1) == "+" || operation.substring(operation.length - 1) == "-" || operation.substring(operation.length - 1) == "*" || operation.substring(operation.length - 1) == "/"){
        operation = String(operation).replace(/.$/, "-");
    }
    else {
        operation = eval(operation) + "-";
    }
    lastOperation = "-";
});


$("#multiplyButton").click(function(){
    inNumber = false;
    // after equals has been pressed
    if (operation == ""){
      operation += $("#display").val();
    } else if (operation.match(/[+,-]?[0-9]+.[0-9]+/)){
      operation = eval(operation);
      $("#display").val(eval(operation));
    }
    if (anotherOp == true){
      operation = String(operation).replace(/.$/, "*");
    }
    anotherOp = true;
    if (operation.substring(operation.length - 1) == "+" || operation.substring(operation.length - 1) == "-" || operation.substring(operation.length - 1) == "*" || operation.substring(operation.length - 1) == "/"){
        operation = String(operation).replace(/.$/, "*");
    }
    else {
        operation = eval(operation) + "*";
    }
    lastOperation = "*";
});



$("#divideButton").click(function(){
  inNumber = false;
  // after equals has been pressed
  if (operation == ""){
    operation += $("#display").val();
  } else if (operation.match(/[+,-]?[0-9]+.[0-9]+/)){
    operation = eval(operation);
    $("#display").val(eval(operation));
  }
  if (anotherOp == true){
    operation = String(operation).replace(/.$/, "/");
  }
  anotherOp = true;
  if (operation.substring(operation.length - 1) == "+" || operation.substring(operation.length - 1) == "-" || operation.substring(operation.length - 1) == "*" || operation.substring(operation.length - 1) == "/"){
      operation = String(operation).replace(/.$/, "/");
  }
  else {
      operation = eval(operation) + "/";
  }
  lastOperation = "/";
});


$("#clearButton").click(function(){
  $("#display").val("");
  inNumber = false;
  operation = "";
  anotherOp = false;
  lastOperation = "";
});

$("#equalsButton").click(function(){
  if (equalsHitted == true) {
    operation  = res + lastOperation + lastOperator;
    $("#display").val(eval(operation));
    res = eval(operation);
  } else {
    $("#display").val(eval(operation));
    res = eval(operation).toString();
    operation = "";
    equalsHitted = true;
    inNumber = false;
    anotherOp = false;
  }
});
